from django.shortcuts import render
from receipts.models import Receipt


# Create your views here.
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user.id)
    context = {
        "receipts_list": receipts,
    }
    return render(request, "receipts/list.html", context)
